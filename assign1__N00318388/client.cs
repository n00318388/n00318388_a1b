﻿using System;
namespace assign1__N00318388
{
    public class Client
    {
        private string clientName;
        private string clientPhone;
        private string clientEmail;

        public Client()
        {

        }
        public string ClientName
        {
            get { return ClientName; }
            set { ClientName = value; }
        }
        public string ClientPhone
        {
            get { return ClientPhone; }
            set { ClientPhone = value; }
        }
        public string ClientEmail
        {
            get { return ClientEmail; }
            set { ClientEmail = value; }
        }

    }
}