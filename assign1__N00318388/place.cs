﻿using System;
using System.Collections.Generic;

namespace assign1__N00318388
{
    public class Place
    {
        public string size;

        public int rooms;

        public List<string> services;

        public Place(List<string> s, string si, int r)
        {
            services = s;
            size = si;
            rooms = r;
        }

    }
}