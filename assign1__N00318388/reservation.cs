﻿using System;
namespace assign1__N00318388
{
    public class Reservation
    {
        public Client client;
        public Place place;
        private Client newclient;

        public Client Client { get; }
        public Place Place { get; }

        public Reservation(Client c, Place p)
        {
            Client = c;
            Place = p;

        }

        public Reservation(Client newclient)
        {
            this.newclient = newclient;
        }

        public string PrintReceipt()
        {
            string receipt = "Reservation Receipt:<br>";
            receipt += "Your total is :" + CalculateOrder().ToString() + "<br/>";
            receipt += "Name: " + client.ClientName + "<br/>";
            receipt += "Email: " + client.ClientEmail + "<br/>";
            receipt += "Phone Number: " + client.ClientPhone + "<br/>";

            receipt += "Room Size: " + rooms.size + "<br/>";
            receipt += "Extra Services: " + String.Join(" ", place.services.ToArray()) + "<br/>";

            return receipt;
        }

        public double CalculateOrder()
        {
            double total = 0;
            if (place.size == "apartment")
            {
                total = 250;
            }
            else if (place.size == "House")
            {
                total = 350;
            }
            else if (place.size == "Business")
            {
                total = 500;
            }

            total += place.services.Count() * 75;

            return total;
        }

    }
}