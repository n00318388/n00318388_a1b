﻿using System;
using System.ComponentModel.Design;
using System.Web;
using System.Web.Services.Description;
using System.Web.UI;

namespace assign1__N00318388
{

    public partial class Default : System.Web.UI.Page
    {

        public void Reservations(object sender, EventArgs args)

        {
            if (Page.IsValid) 
            {
                return;
            }
             info.InnerHtml = "Your Reservation has been confirmed!";

    int rooms = int.Parse(numberRooms.Text);


    string name = clientName.Text.ToString();
    string email = clientEmail.Text.ToString();
    string phone = clientPhone.Text.ToString();
    Client newclient = new Client();

    newclient.ClientName = name;
    newclient.ClientPhone = phone;
    newclient.ClientEmail = email;

   
    List<string> extraservices = new List<string>();

    foreach (Control control in ServiceContainer.Controls)
    {
        if (control.GetType() == typeof(CheckBox))
        {
            CheckBox topping = (CheckBox)control;
            if (extra.Checked)
            {
                extraservices.Add(Service.Text);
            }

        }
    }

    Reservation neworder = new Reservation(newclient);
 
    OrderRes.InnerHtml = neworder.PrintReceipt();
        }
    }
}
